;(function(){
    // =============   Technologies hover =============
    $('.tech-item').hover(onMouseIn, function() {});

    function onMouseIn() {
        var imgs = $(this).find('img');
        var imgPB = imgs[0];
        var imgColor = imgs[1];
        $(imgPB).hide();
        $(imgColor).show();
    }

    function onMouseOut() {
        var imgs = $(this).find('img');
        var imgPB = imgs[0];
        var imgColor = imgs[1];
        $(imgColor).hide();
        $(imgPB).show();
    }

    function getAssetName(path) {
        return path.replace(/^.*[\\\/]/, '');
    }

    // =============   Menu settings   =============
    $(document).mouseup(closeMenuIfOpen);

    $('#menuToggle, .menu-close').on('click', toggleMenu);

    function toggleMenu() {
        $('#menuToggle').toggleClass('active');
        $('body').toggleClass('body-push-toleft');
        $('#theMenu').toggleClass('menu-open');
    }

    function closeMenuIfOpen(event) {
        var container = $(".menu-wrap");

        if (!container.is(event.target) // if the target of the click isn't the container...
            && container.has(event.target).length === 0 // ... nor a descendant of the container
            && $('#theMenu').hasClass('menu-open'))
        {
            closeMenu();
        }
    }

    function closeMenu() {
        $('#menuToggle').removeClass('active');
    	$('body').removeClass('body-push-toleft');
    	$('#theMenu').removeClass('menu-open');
    }
})(jQuery)
